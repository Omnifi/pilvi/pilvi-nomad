# Pilvi Nomad

_Pilvi Nomad_ provides package management tools for installing and configuring [Nomad](https://www.nomadproject.io) clusters. It is concieved of as a component of a self-sovereign cloud stack, operating as a command-line interface (CLI), web interface, and a sub-component of the _Pilvi_ cloud management stack. 

## Origin of name

Pilvi means "cloud" in Finnish. The name was chosen based on the origin of the project steward ([Omnifi Foundation](https://omnifi.foundation) being founded originally in Finland) and the purpose of the project (focused on cloud development and orchestration). 